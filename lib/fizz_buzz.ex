defmodule FizzBuzz do
  @moduledoc """
  Imprime números de 1 a 100,
  porém Fizz para multiplos de 3 e
  Buzz para multiplos de 5 e 
  FizzBuzz para multiplos de 3 e 5
  """

  @doc """
  Hello world.

  ## Examples

      iex> FizzBuzz.imprimir(1)
      1

  """
  def imprimir(numero) do
    cond do
      multiplo_de3(numero) && multiplo_de5(numero) -> "FizzBuzz"
      multiplo_de3(numero) -> "Fizz"
      multiplo_de5(numero) -> "Buzz"
      true -> numero
    end
  end

  defp multiplo_de3(numero), do: rem(numero, 3) == 0
  defp multiplo_de5(numero), do: rem(numero, 5) == 0

  def lista() do
    Enum.reduce(2..100, 1, fn i, acc -> "#{acc},#{imprimir(i)}" end) |> IO.inspect()
  end
end
