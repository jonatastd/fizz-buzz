defmodule FizzBuzzTest do
  use ExUnit.Case
  doctest FizzBuzz

  test "deve imprimir 1 quando for 1" do
    assert FizzBuzz.imprimir(1) == 1
  end

  test "deve imprimir 2 quandor for 2" do
    assert FizzBuzz.imprimir(2) == 2
  end

  test "deve imprimir Fizz quando for multiplo de 3" do
    assert FizzBuzz.imprimir(3) == "Fizz"
  end

  test "deve imprimir 4 quando for 4" do
    assert FizzBuzz.imprimir(4) == 4
  end

  test "deve imprimir FizzBuzz quando for multiplo de 3 e 5" do
    assert FizzBuzz.imprimir(15) == "FizzBuzz"
  end

  test "deve uma lista de 1 a 100" do
    assert FizzBuzz.lista() ==  "1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,FizzBuzz,16,17,Fizz,19,Buzz,Fizz,22,23,Fizz,Buzz,26,Fizz,28,29,FizzBuzz,31,32,Fizz,34,Buzz,Fizz,37,38,Fizz,Buzz,41,Fizz,43,44,FizzBuzz,46,47,Fizz,49,Buzz,Fizz,52,53,Fizz,Buzz,56,Fizz,58,59,FizzBuzz,61,62,Fizz,64,Buzz,Fizz,67,68,Fizz,Buzz,71,Fizz,73,74,FizzBuzz,76,77,Fizz,79,Buzz,Fizz,82,83,Fizz,Buzz,86,Fizz,88,89,FizzBuzz,91,92,Fizz,94,Buzz,Fizz,97,98,Fizz,Buzz"
  end

end
